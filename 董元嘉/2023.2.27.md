笔记
app.use(async (ctx,next)=>{
    await next();
    ctx.response.type = 'text/html';
    ctx.response.body = '又是开摆的一天';
});
这个是koa的一个中间件的基础结构，由很多个这样的中间件来组成处理链，以方便处理数据的同时利于修改和调试

app.use(async (ctx,next)=>{
    console.log(`${ctx.request.method} ${ctx.request.url}`) //打印URL
    await next(); //调用下一个中间件
});

app.use(async (ctx,next)=>{
    const start = new Date().getTime(); // 当前时间
    await next();
    const ms = new Date().getTime() - start; // 耗费时间
    console.log(`Time: ${ms}ms`);
});

app.use(async (ctx,next)=>{
    await next();
    ctx.response.type = 'text/html';
    ctx.response.body = '其实没有这么多开摆，只是想不想做';
});

这是koa整个的一个运行结构，处理链在没个await next();的地方已经开始运行下一个中间件了，在最后才将await下的代码结果展示;

在这样一个中间件的系统中我们可以设置一个访问权限：
app.use(async (ctx,next)=>{
    if(await checkUserPermission(ctx)){
        await next();
    }else{
        ctx.response.status = 403;
    }
});

处理URL
const Koa = require('Koa');
const router = require('koa-router')(); //用这个记得要在配置文件中加 "koa-router": "12.0.0";
let app = new Koa();

router.get('/',async (ctx,next)=>{
    ctx.response.body = "摆！";
    await next();
});

router.get('/aboutme', async (ctx,next)=>{
    ctx.response.body = '完蛋';
    await next();
});

app.use(router.routes());

let port = 3000;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);